from django.contrib import admin
from django.urls import path, include



urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/thai/', include('thlotto.urls')),
    path('api/laos/', include('laolotto.urls')),
]
